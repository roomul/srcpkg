#!/bin/bash
#

SOURCE="./void-packages/srcpkgs"
TARGET="./srcpkg"

log(){
	printf " -> $1 \n"
}

err(){
	log "ERROR: $1"
	exit 1
}

copys(){
	for pkg in $PKGS ; do
		log "Copy $pkg -> $SOURCE/$pkg"
		cp -r $TARGET/$pkg $SOURCE/
	done
	sync
	exit 0
}

main(){
	trap "exit" INT ERR

	if [ -f "$PWD/list" ]; then
    PKGS=$(cat $PWD/list)
  else
    err "Not found 'list'."
  fi

	if [ ! "$(ls -A $TARGET)" ]; then
		err "Directory '$TARGET' not set."
	fi

	case "$1" in
		c|copy) copys ;;
	esac

	for pkg in $PKGS ; do
		if [ -d "$SOURCE/$pkg" ]; then
			log "Copy $SOURCE/$pkg -> $TARGET/$pkg"
			cp -R $SOURCE/$pkg $TARGET
		else
			log "Package $pkg not found."
		fi
	done

	sync
	exit 0
}

main "$@"
